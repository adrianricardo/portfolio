Main = {
	init: function(){
		var size = 100;
		var target = "";
		var delay = false; //otherwise scrollspy will trigger every time a scroll is trigger through 
		//JS, this way its only triggered on the last item in the scroll journey
		var speed = 500;
		window.offset = -11;
		window.scrollSet = 7;

		//scrollspy
		$('#nav').on('activate.bs.scrollspy', function (e) {
		  	var id = $(e.target).data('id');
		  	$('.entry').removeClass('active');
		  	$("#"+id).addClass('active');
			if(!Main.delay){
				scrollSidebar(e.target);
				
				
			}else{
				console.log("main is still true?");
			}
		})

		//scrollsnap
		$(document).ready(function() {
            $(document).scrollsnap({
                snaps: '.snap',
                proximity: 100,
                duration: 100
            });
            if(!Modernizr.touch){
	            $(document).ready(function(){
	                var options = new Object();
	                options.showThumbs = false;
	                options.direction = 'vertical';
	                $("#sidebar").overscroll(options);

	            });
           	}
        });

		//fastclick
		window.addEventListener('load', function() {
		    FastClick.attach(document.body);
		}, false);

		function scrollSidebar(target){
			$('#sidebar').scrollTo( target, speed, {
					easing:'easeInOutExpo', 
					onAfter: function() { 
						speed = speed/.7;
					}
				});
				speed = speed*.7;
		}

		//on resize 
		$(window).resize(function() {
		    //recalc the bottom padding
		  	Main.CalcBottomPadding();
		  	//refresh scrollspy
			$('[data-spy="scroll"]').each(function () {
			  var $spy = $(this).scrollspy('refresh')
			})
		});

		$('.url button').on('click',function(){
			var url = $(this).data('url');
			window.open("http://"+url);
		})

		//intialize fancy box
		$(document).ready(function() {
			$(".fancybox").fancybox({
				afterLoad: function() {
					var link = $(this.element).data('link');
					if(link.length){
				        this.title = this.title + ' <a href="' + link + '" target="_blank">View it</a>';
				    }
			    },
			    helpers : {
			        overlay : {
			            css : {
			                'background' : 'rgba(255, 255, 255, 0.95)'
			            }
			        }
			    }
			});
		});



		$('.entry-trigger').on('click','a',function(e){
			e.preventDefault();
			Main.delay = true;
			$('.container').removeClass('open');
			var id=$(this).parent()[0].getAttribute('data-id');
			$('.entry-trigger').removeClass('active');		
			$(this).addClass('active');
			

			$('html, body').animate({
				scrollTop: $("#"+id).offset().top - window.scrollSet
			}, '500', 'linear', function() {
				Main.delay = false;
				scrollSidebar($("#trigger-"+id));
			});
 		})

 		$('.entry-trigger').on('mouseenter',function(){
 			$(this).removeClass('faded');
 		});
 		$('.entry-trigger').on('mouseleave',function(){
 			$(this).addClass('faded');
 		});

 		Main.initCollapseHighlightChildren();
 		Main.CalcBottomPadding();
 		Main.initMobilePanel();
	},
	initCollapseHighlightChildren: function(){
		//insert show more links
		$('.entry .content ul ul').each(function(index){
			$(this).prev().append(" <a href='#' class='highlight-toggle'></a>")
		})

		//toggle children
		$('.entry .content').on('click','.highlight-toggle',function(e){
			e.preventDefault();
			$(this).toggleClass('open');
			$list = $(this).parent().next();
			$list.toggle();
		})
	},
	initMobilePanel: function(){
		$('#mobile-nav i').on('click',function(){
			$('.container').toggleClass('open');
		})
	},
	CalcBottomPadding: function(){
		window_size = $(window).height();
		elem_height = $( ".entry:last-child" ).height();
		$( ".entry:last-child" ).css('padding-bottom',window_size-elem_height);
		if ($(window).width() > 640) {
			window.offset = -11;
			window.scrollSet = 7;
			elem_height = $( ".entry-trigger:last-child" ).height();
			$( ".entry-trigger:last-child" ).css('padding-bottom',window_size-elem_height);
		}else{
			window.offset = -55;
			window.scrollSet = 50;
		}
	}
}
