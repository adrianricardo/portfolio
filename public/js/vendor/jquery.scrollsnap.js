(function( $ ) {

    $.fn.scrollsnap = function( options ) {

        var settings = $.extend( {
            'direction': 'y',
            'snaps' : '*',
            'proximity' : 12,
            'offset' : 0,
            'duration' : 200,
            'easing' : 'swing',
        }, options);

        var leftOrTop = settings.direction === 'x' ? 'Left' : 'Top';

        return this.each(function() {

            var scrollingEl = this;
            console.log

            if (scrollingEl['scroll'+leftOrTop] !== undefined) {
                // scrollingEl is DOM element (not document)
                $(scrollingEl).css('position', 'relative');

                $(scrollingEl).bind('scrollstop', function(e) {

                    var matchingEl = null, matchingDy = settings.proximity + 1;

                    $(scrollingEl).find(settings.snaps).each(function() {
                         var snappingEl = this,
                            dy = Math.abs(snappingEl['offset'+leftOrTop] + window.offset - scrollingEl['scroll'+leftOrTop]);

                        if (dy <= settings.proximity && dy < matchingDy) {
                            matchingEl = snappingEl;
                            matchingDy = dy;
                        }
                    });

                    if (matchingEl) {
                         var endScroll = matchingEl['offset'+leftOrTop] + window.offset,
                            animateProp = {};
                        animateProp['scroll'+leftOrTop] = endScroll;
                        if ($(scrollingEl)['scroll'+leftOrTop]() != endScroll) {
                            $(scrollingEl).animate(animateProp, settings.duration, settings.easing);
                        }
                    }

                });

            } else if (scrollingEl.defaultView) {
                // scrollingEl is DOM document
                 $(scrollingEl).bind('scrollstop', function(e) {

                    var matchingEl = null, matchingDy = settings.proximity + 1;

                    $(scrollingEl).find(settings.snaps).each(function() {
                        var snappingEl = this,
                            dy = Math.abs(($(snappingEl).offset()[leftOrTop.toLowerCase()] + window.offset) - scrollingEl.defaultView['scroll'+settings.direction.toUpperCase()]);
                         if (dy <= settings.proximity && dy < matchingDy) {
                             matchingEl = snappingEl;
                            matchingDy = dy;
                        }
                    });

                    if (matchingEl) {
                        var id = matchingEl.getAttribute('data-id');
                        console.log(id);
                        //$("#sidebar").scrollTop($("#trigger-"+id).position().top);
                        //console.log('scrolling to trigger '+id+': '+$("#trigger-"+id).offset().top);
                        //$("#trigger-"+id).scrollintoview({offset: -100});
                        console.log('sfsdfsf');
                        //$('#sidebar').scrollTo("#trigger-"+id,500, {offset:-6});
                       // $('.entry-trigger').removeClass('active');      
                       // $('#trigger-'+id).addClass('active');
                        var endScroll = $(matchingEl).offset()[leftOrTop.toLowerCase()] + window.offset,
                            animateProp = {};
                        animateProp['scroll'+leftOrTop] = endScroll;
                        if ($(scrollingEl)['scroll'+leftOrTop]() != endScroll) {
                             $('html, body').animate(animateProp, settings.duration, settings.easing);
                        }
                    }

                });
            }

        });

    };

})( jQuery );
