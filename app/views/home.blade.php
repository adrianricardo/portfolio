@extends('layout')

@section('content')
    <div id="mobile-nav">
        <i id="trigger" class="open fa fa-align-justify"></i>
        <i class="cancel fa fa-times"></i>
        <div class="info">
            <img src="img/headshot.jpg">
            <span>Adrian Tavares</span>
        </div>
    </div>
   <div id="wrap">
                <div id="sidebar">
                    <div class="content">
                        <?$photo_array = array(); ?>

                        <?php foreach ($photos as $photo) {
                            if(!isset($photo_array[$photo->entry_id])){
                                $photo_array[$photo->entry_id] = array();
                            }
                            array_push($photo_array[$photo->entry_id], $photo);
                        }?>
                        <?php foreach ($references as $reference) {
                            if(!isset($reference_array[$reference->entry_id])){
                                $reference_array[$reference->entry_id] = array();
                            }
                            array_push($reference_array[$reference->entry_id], $reference);
                        }?>
                        <div id="nav">
                            <ul class="nav">
                            	<?php foreach ($entries as $entry) { ?>
                                    <li id="trigger-<?php echo $entry['id']; ?>" class="entry-trigger faded" data-id="<?php echo $entry['id']; ?>">
                                        <a href="#<?php echo $entry['id']; ?>" style="">
                                            <div class="date">
                                                <div class="triangle-right"></div>
                                                <span class="title">
                                                    <?php echo date("M Y", mktime(0,0,0,$entry['month_start'],1,$entry['year_start']));?>
                                                     -
                                                    <?php   if($entry['year_end'] != "" && $entry['month_end'] != ""){
                                                                echo date("M Y", mktime(0,0,0, $entry['month_end'], 1, $entry['year_end']));
                                                            }else if($entry['year_end'] != ""){
                                                                echo date("Y", mktime(0,0, 0, 1, 1,$entry['year_end']));
                                                            }else if($entry['month_end'] != ""){
                                                                echo date("M", mktime(0,0, 0, $entry['month_end'], 1,1));
                                                            }else{
                                                                echo "NOW";
                                                            }
                                                    ?>
                                                </span>
                                            </div>
                                            <?php if(!empty($entry['company'])){ ?> 
            	                                <div class="name">
            	                                    <?php echo $entry['company']; ?> 
            	                               
            	                                </div>
            	                            <?php } ?>
                                            <?php if(!empty($entry['tagline'])){ ?> 
                                                <div class="tagline">
                                                    <?php echo $entry['tagline']; ?> 
                                                </div>
                                            <?php } ?>
                                            <?php if(!empty($entry['location'])){ ?> 
                                                <div class="location">
                                                    <?php echo $entry['location']; ?> 
                                                </div>
                                            <?php } ?>
                                            <?php if(!empty($entry['url'])){ ?> 
                                                <div class="url">
                                                    <button type="button" data-url="<?php echo $entry['url']; ?>"><?php echo $entry['url']; ?></button> <i class="fa fa-external-link-square"></i> 
                                                </div>
                                            <?php } ?>
                                        </a>
                                    </li>
                                <? } ?>
                            </ul>
                        </div>
                     </div>
                </div>
            </div>
            <div class="wrapper">
                <div id="entry-container">
                    <div id="meter">
                        <div class="filler">
                    		<?php foreach ($entries as $entry) { ?>
                                <div id="<?php echo $entry['id']; ?>" class="entry">
                                    
                                    <div class="clearfix">
                                        <div class="title snap" data-id="<?php echo $entry['id']; ?>">
                                            <div class="triangle-left">
                                            </div>
                                            <?php echo $entry['company']; ?>
                                        </div>
                                        <div class="tagline"><?php echo $entry['tagline']; ?></div>
                                    </div>
                                    @if( $entry['tags'])
                                        <ul class="tags">
                                            <?php $tags = explode(",", $entry['tags']); ?>
                                            <?php foreach ($tags as $tag) : ?>
                                                <li><?=$tag?></li>
                                            <?php endforeach; ?>
                                        </ul>
                                    @endif
                                    <div class="description"><?php echo $entry['description']; ?></div>
                                    <div class="content"><?php echo $entry['content']; ?></div>
                                    <?php if(isset($photo_array[$entry['id']])) : ?>
                                        <ul class="photos">
                                            <?php foreach ($photo_array[$entry['id']] as $photo) : ?>
                                                <li>
                                                    <a class="fancybox" rel="gallery<?=$entry['id']?>" href="img/exhibits/<?=$photo->filename?>" title="<?=$photo->caption?>" data-link="<?=$photo->link?>">
                                                        <img src="img/exhibits/thumbs/<?=$photo->filename?>" alt="<?=$photo->caption?>" />
                                                    </a>
                                                </li>
                                            <?endforeach?>
                                        </ul>
                                    <?php endif; ?>
                                    <?php if(isset($reference_array[$entry['id']])) : ?>
                                        <ul class="references">
                                            <?php foreach ($reference_array[$entry['id']] as $reference) : ?>
                                                <li>
                                                    <img src="img/references/<?=$reference->photo?>" />
                                                    <div class="outter">
                                                        <div class="inner">
                                                            <span class="body"><?=$reference->body?></span>
                                                            <span class="name">
                                                                <a href="<?=$reference->url?>"><?=$reference->name?></a></span>
                                                            <span class="role"><?=$reference->title?> @ <?=$reference->company?></span>
                                                            <span class="source">from <?=$reference->source?></span>
                                                        </div>
                                                    </div>
                                                </li>
                                            <?endforeach?>
                                        </ul>
                                    <?php endif; ?>
                                </div>
                            <? } ?>
                        </div>
                    </div>
                </div>
            </div>
@stop