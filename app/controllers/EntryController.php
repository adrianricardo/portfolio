<?php

class EntryController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getIndex()
	{
		$data['entries'] = Entry::orderBy(DB::raw('(case when year_end is null then 0 else 1 end)'))
			->orderBy(DB::raw('ifnull(year_end, year_start) DESC, ifnull(month_end, month_start) DESC, id'))
			->where('visible',1)
			->get();
		$data['photos'] = DB::table('photos')->get();
		$data['references'] = DB::table('references')->get();
 		return View::make('home',$data);
	}

}